# Vaccination Service
The vaccination service exists to register administered Covid-19 vaccinations.

## Installing Helm Chart locally
For development purposes, you may need to modify and install the modified Helm Chart locally. This can be done with the
following command:
```
helm upgrade --install release charts/vaccination-service
```